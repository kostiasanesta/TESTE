package com.volkov.application;

public class Application {
	private Changer changer;
	private Person person;
	
	public Application(){
		changer = new Changer();
		person = new Person("kostia", 5);
	}
	
	public static void main(String[] args){
		
		Application app = new Application();
		app.runTheApp();
		
	}

	private void runTheApp() {
		person.setName(changer.format(person));
		changer.timeFormat(person);
		person.setAge(changer.ageFormat(person));
		System.out.println(person.getName()+" age "+person.getAge());
	}

}

package com.volkov.application;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;

public class Changer {

    public String format(Object obj) {
        String name = "nety";
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Name.class)) {
                try {
                    name = (String) field.get(obj);
                    String ch = String.valueOf(name.charAt(0));
                    name = name.replaceFirst(ch, ch.toUpperCase());
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return name;
    }

    public void timeFormat(Object obj) {
        Method[] methods = obj.getClass().getMethods();
        for (Method meth : methods) {
            if (meth.isAnnotationPresent(Timed.class)) {
                System.out.println(meth.getName() + " " + new Date(System.currentTimeMillis()));
            }
        }
    }

    public int ageFormat(Object obj) {
        Field[] fields = obj.getClass().getDeclaredFields();
        int age = 0;
        for(Field field:fields){
            if(field.isAnnotationPresent(Age.class)){
                try {
                    Age taskAge = field.getAnnotation(Age.class);
                    age = (int) field.get(obj);
                    if (age<taskAge.min()){
                        age = taskAge.min();
                    }else if(age>taskAge.max()){
                        age = taskAge.max();
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        return age;
    }
}
